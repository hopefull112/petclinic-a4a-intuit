package com.intuit.demo.repository;

import com.intuit.demo.domain.Owner;
import com.intuit.demo.domain.Vet;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface VetRepository extends Repository<Vet, Integer> {

    void save(Vet vet);

    List<Vet> findAll();

    Vet findById(Integer vetId);

}
