import axios from "axios";
import {flashErrorMessage} from "redux-flash";
import moment from "moment";
import * as ActionTypes from "../actiontypes/VetActionTypes"

export function fetchVets(lastname = "") {
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_VETS});

        axios.get("http://localhost:8082/api/vets?lastName="+lastname)
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_VETS_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function fetchVet(vetId) {
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_VET});

        axios.get("http://localhost:8082/api/vets/"+vetId)
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_VET_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function fetchVetVisits(vetId) {
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_VET_VISITS});
        axios.get("http://localhost:8082/api/vets/"+vetId+"/visits")
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_VET_VISITS_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function createVet(data) {
    return function (dispatch) {
        dispatch({type: ActionTypes.CREATE_VET})
        axios.post("http://localhost:8082/api/vets", data)
            .then((response) => {
                dispatch({type: ActionTypes.CREATE_VET_FULFILLED, payload: response.data})
            })
            .catch(err => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function fetchVetAvailableTimes(vetId, visitDate) {
    const visitDateStr = moment(visitDate).format("MM-DD-YYYY");
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_VET_AVAILABLE_TIMES});
        axios.get("http://localhost:8082/api/vets/"+vetId+"/availability/"+visitDateStr)
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_VET_AVAILABLE_TIMES_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                console.log(err)
                dispatch(flashErrorMessage("API   Error : " + err.message));
            })
    }
}