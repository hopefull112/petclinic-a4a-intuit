package com.intuit.demo.domain;

public enum VisitStatus {

    COMPLETED,
    SCHEDULED,
    CANCELED
}