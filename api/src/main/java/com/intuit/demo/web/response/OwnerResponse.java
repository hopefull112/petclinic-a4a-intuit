package com.intuit.demo.web.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class OwnerResponse {

    private Integer id;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String telephone;
    private List<OwnerPetResponse> pets = new ArrayList<>();

    public static OwnerResponse create(Integer id, String firstName, String lastName) {
        OwnerResponse dto = new OwnerResponse();
        dto.setId(id);
        dto.setFirstName(firstName);
        dto.setLastName(lastName);
        return dto;
    }
}
