package com.intuit.demo.web.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OwnerRequest {

    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String telephone;

}
