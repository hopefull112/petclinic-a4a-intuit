package com.intuit.demo.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "vets")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Long.class)
public class Vet extends Person {

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "vet_specialties", joinColumns = @JoinColumn(name = "vet_id"),
            inverseJoinColumns = @JoinColumn(name = "specialty_id"))
    private List<Specialty> specialities = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vet")
    private List<Visit> visits = new ArrayList<>();

    public List<Specialty> getSpecialities() {
        return specialities;
    }

    public int getNrOfSpecialties() {
        return getSpecialities().size();
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public void addSpecialty(Specialty specialty) {
        getSpecialities().add(specialty);
    }

    public void addVisit(Visit visit) {
        getVisits().add(visit);
        visit.setVet(this);
    }

}
