package com.intuit.demo.service.impl;

import com.intuit.demo.domain.Specialty;
import com.intuit.demo.domain.Vet;
import com.intuit.demo.domain.Visit;
import com.intuit.demo.domain.VisitStatus;
import com.intuit.demo.repository.SpecialtyRepository;
import com.intuit.demo.repository.VetRepository;
import com.intuit.demo.repository.VisitRepository;
import com.intuit.demo.service.VetService;
import com.intuit.demo.web.request.VetRequest;
import com.intuit.demo.web.response.AvailabilityResponse;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VetServiceImpl implements VetService {

    private VetRepository vetRepository;
    private SpecialtyRepository specialtyRepository;
    private VisitRepository visitRepository;
    private AvailabilitiesProvider availabilitiesProvider;

    public VetServiceImpl(VetRepository vetRepository, SpecialtyRepository specialtyRepository, VisitRepository visitRepository, AvailabilitiesProvider availabilitiesProvider) {
        this.vetRepository = vetRepository;
        this.specialtyRepository = specialtyRepository;
        this.visitRepository = visitRepository;
        this.availabilitiesProvider = availabilitiesProvider;
    }

    @Override
    public List<Vet> getVets() {
        return vetRepository.findAll();
    }

    @Override
    public Vet getVet(Integer vetId) {
        return vetRepository.findById(vetId);
    }

    @Override
    public Vet addVet(VetRequest req) {
        Vet vet = new Vet();
        vet.setFirstName(req.getFirstName());
        vet.setLastName(req.getLastName());
        addSpecialities(req, vet);
        vetRepository.save(vet);
        return vet;
    }

    private void addSpecialities(VetRequest req, Vet vet) {
        if(req.getSpecialityIds() != null) {
            String[] sIds = req.specialityIds.split(",");
            for(String sid : sIds) {
                Specialty sp = specialtyRepository.findById(Integer.parseInt(sid.trim()));
                vet.addSpecialty(sp);
            }
        }
    }

    @Override
    public List<AvailabilityResponse> retrieveAvailableTimes(Integer vetId, LocalDate visitDate) {
        LocalDateTime start = LocalDateTime.of(visitDate, LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(visitDate.plusDays(1), LocalTime.of(0, 0));
        List<Visit> visits = visitRepository.findAllByVet_IdAndDateBetweenAndVisitStatus(vetId, start, end, VisitStatus.SCHEDULED.name());
        List<LocalDateTime> unAvailableTimes = visits.stream().map(Visit::getDate).collect(Collectors.toList());
        return availabilitiesProvider.calculateAvailableTimes(visitDate, unAvailableTimes);
    }
}