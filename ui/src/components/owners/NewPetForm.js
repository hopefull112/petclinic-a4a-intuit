import React from 'react'
import { Field, reduxForm } from 'redux-form'
import {
    minLength2,
    renderDatePicker,
    renderField, renderSelect,
    required, alphaNumeric
} from "../common/ReduxFormCustomRenders"

const NewPetForm = (props) => {

    const {handleSubmit, reset, submitting} = props;
    const petTypesOptions = [
        {value: '1', label: 'Bird'},
        {value: '2', label: 'Cat'},
        {value: '3', label: 'Dog'},
        {value: '4', label: 'Hamster'},
        {value: '5', label: 'Lizard'},
        {value: '6', label: 'Snake'}
    ];

    return(
        <form onSubmit={handleSubmit}>
            <div className="input-group mb-3">

                <div className="col-3 pl-0">
                    <Field name="name" component={renderField} type="text"
                           placeholder="Name" className="form-control"
                           validate={[required, minLength2, alphaNumeric]} autoComplete="off"/>
                </div>
                <div className="col-3">
                    <Field name="birthDate" component={renderDatePicker} type="text"
                           className="form-control"
                           autoComplete="off"
                           placeholderText="Birth date"
                           dateFormat="MM/dd/yyyy"
                           maxDate={new Date()}
                           validate={[required]}/>
                </div>
                <div className="col-3">
                    <Field name="typeId" component={renderSelect}
                           options={petTypesOptions} isMulti={false}
                           placeholder="Select a type"
                           validate={[required]}
                    />
                </div>
                <div className="col-1">
                    <button type="button" onClick={reset}
                            className="btn btn-default">Reset
                    </button>
                </div>
                <div className="col-1 mr-2">
                    <button type="submit" disabled={submitting}
                            className="btn btn-default">Submit
                    </button>
                </div>
            </div>
        </form>
    )
}

export default reduxForm({
    form: 'new-pet-form'  // a unique identifier for this form
})(NewPetForm)