package com.intuit.demo.service.impl;

import com.intuit.demo.domain.Owner;
import com.intuit.demo.domain.Pet;
import com.intuit.demo.domain.PetType;
import com.intuit.demo.repository.OwnerRepository;
import com.intuit.demo.repository.PetRepository;
import com.intuit.demo.repository.PetTypeRepository;
import com.intuit.demo.service.OwnerService;
import com.intuit.demo.web.request.OwnerRequest;
import com.intuit.demo.web.request.PetRequest;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class OwnerServiceImpl implements OwnerService {

    private PetRepository petRepository;
    private PetTypeRepository petTypeRepository;
    private OwnerRepository ownerRepository;

    public OwnerServiceImpl(OwnerRepository ownerRepository, PetRepository petRepository, PetTypeRepository petTypeRepository) {
        this.ownerRepository = ownerRepository;
        this.petRepository = petRepository;
        this.petTypeRepository = petTypeRepository;
    }

    public Collection<Owner> getOwners(String lastName) {
        return  (lastName == null || lastName.isEmpty()) ? ownerRepository.findAll() : ownerRepository.findByLastNameLike("%"+lastName+"%");
    }

    @Override
    public Owner getOwner(Integer ownerId) {
        return ownerRepository.findById(ownerId);
    }

    public void addOwner(OwnerRequest request) {
        Owner owner = new Owner();
        owner.setFirstName(request.getFirstName());
        owner.setLastName(request.getLastName());
        owner.setAddress(request.getAddress());
        owner.setCity(request.getCity());
        owner.setTelephone(request.getTelephone());
        ownerRepository.save(owner);
    }

    @Override
    public Pet addPet(PetRequest petRequest) {
        Owner owner = ownerRepository.findById(petRequest.getOwnerId());
        Pet pet = new Pet();
        PetType petType = petTypeRepository.findById(petRequest.getTypeId());
        pet.setType(petType);
        pet.setName(petRequest.getName());
        pet.setBirthDate(petRequest.getBirthDate());
        owner.addPet(pet);
        petRepository.save(pet);
        return pet;
    }

    @Override
    public Pet getOwnerPet(Integer ownerId, Integer petId) {
        return petRepository.findById(petId);
    }
}
