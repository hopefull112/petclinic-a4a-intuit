import React from "react";
import {Field} from 'redux-form';
import {required} from "../common/ReduxFormCustomRenders";

const ListAvailableTimes = props => {

        if (!props.availableTimes) {
            return <i>No time slots available.</i>
        }

        const mappedTimes = props.availableTimes.map(time =>
                <span key={time.value}>
          <Field
              name="visitTime"
              component="input"
              type="radio"
              value={time.value}
              validate={[required]}
          /> {time.label} <br/>
        </span>
        );

        return (
            <div className="col-12">
                {mappedTimes}
            </div>
        );
}

export default ListAvailableTimes;
