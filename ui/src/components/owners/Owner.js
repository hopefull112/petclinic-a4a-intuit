import React from 'react'
import {connect} from 'react-redux'
import NewPetForm from './NewPetForm'
import {createPet, fetchOwner} from "../../actions/OwnerActions"
import ListPets from './ListPets'

class Owner extends React.Component {

    componentWillMount() {
        this.props.dispatch(fetchOwner(this.props.match.params.ownerId))
    }

    createPet = (values) => {
        if(values.typeId){
            const typeId = values.typeId.value;
            values.typeId = typeId;
            values.ownerId = this.props.match.params.ownerId
        }
        this.props.dispatch(createPet(values))
    }

    render() {

        const owner = this.props.owner;
        if (!owner) {
            return <h3>No owner loaded</h3>
        }

        return (
            <section>
                <h2>Owner Information</h2>
                <table className='table table-striped'>
                    <tbody>
                    <tr>
                        <th>Name</th>
                        <td><b>{owner.firstName} {owner.lastName}</b></td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>{owner.address}</td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td>{owner.city}</td>
                    </tr>
                    <tr>
                        <th>Telephone</th>
                        <td>{owner.telephone}</td>
                    </tr>
                    </tbody>
                </table>
                <h2 className="mt-4">Add a pet</h2>
                <NewPetForm onSubmit={this.createPet}/>
                <h2 className="mt-4">Pets listing</h2>
                <ListPets pets={this.props.pets} baseUrl={this.props.match.url}/>
            </section>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        owner: state.owners.owner,
        pets: state.owners.ownerPets
    }
}

export default connect(mapStateToProps)(Owner)
