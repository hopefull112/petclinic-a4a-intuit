import React from 'react'
import {connect} from 'react-redux'
import ListVets from './ListVets'
import NewVetForm from './NewVetForm'
import {fetchVets, createVet} from "../../actions/VetActions"

class Vets extends React.Component {

    constructor(props) {
        super(props)
        this.handleCreateVet = this.handleCreateVet.bind(this);
    }

    componentWillMount() {
        this.props.dispatch(fetchVets())
    }

    handleCreateVet(values) {
        if (values.specialityIds) {
            const specialityIds = values.specialityIds.map(
                speciality => speciality.value).join(', ');
            values.specialityIds = specialityIds;
        }
        this.props.dispatch(createVet(values))
    }

    render() {
        const {vets} = this.props.vets;
        return (
            <div>
                <h2> Create Vets </h2>
                <NewVetForm onSubmit={this.handleCreateVet}/>

                <h2 className="mt-4">Vets Listing</h2>
                <ListVets vets={vets} baseUrl={this.props.match.url}/>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        vets: state.vets
    }
}

export default connect(mapStateToProps)(Vets)