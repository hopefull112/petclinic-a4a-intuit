package com.intuit.demo.web.response;

import com.intuit.demo.domain.Specialty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VetResponse {

    private Integer id;
    private String firstName;
    private String lastName;
    private List<Specialty> specialities;
    private List<VisitResponse> visits;

    public static VetResponse create(Integer id, String firstName, String lastName, List<Specialty> specialties) {
        VetResponse dto = new VetResponse();
        dto.setId(id);
        dto.setFirstName(firstName);
        dto.setLastName(lastName);
        dto.setSpecialities(specialties);
        return dto;
    }
}
