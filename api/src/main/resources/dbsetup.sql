insert into SPECIALTIES (id, name) values (1, 'Primary Care');
insert into SPECIALTIES (id, name) values (2, 'Radiology');
insert into SPECIALTIES (id, name) values (3, 'Surgery');

insert into TYPES (id, name) values (1, 'Bird');
insert into TYPES (id, name) values (2, 'Cat');
insert into TYPES (id, name) values (3, 'Dog');
insert into TYPES (id, name) values (4, 'Hamster');
insert into TYPES (id, name) values (5, 'Lizard');
insert into TYPES (id, name) values (6, 'Snake');

insert into owners(id, first_name, last_name, address, city, telephone) values (1, 'Anna', 'Jones', '657 Glen Dale', 'San Diego', '8586512525');
insert into owners(id, first_name, last_name, address, city, telephone) values (1, 'Jeff', 'Sadlo', '456 Booke Lane', 'San Diego', '6197582526');
insert into owners(id, first_name, last_name, address, city, telephone) values (1, 'Mary', 'Kom', '633 Cobot Ave', 'San Diego', '8587879696');
insert into owners(id, first_name, last_name, address, city, telephone) values (1, 'Sarah', 'Cooper', '562 Bliss Ave', 'San Diego', '8581254569');
