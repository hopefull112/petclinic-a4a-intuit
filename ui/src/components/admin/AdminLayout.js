import React from 'react';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import Routes from '../routes/Routes';
import Container from 'react-bootstrap/Container'
import Header from './Header'
import FlashMessages from "../common/FlashMessages"

export const AdminLayout = () => {
    return (
        <BrowserRouter>
            <div>
                <Header />

                <Container className="py-4">
                    <FlashMessages/>
                    <Routes /> {/* This is where components will be rendered  */}
                </Container>

                <footer className="text-center">
                    <p>This is footer</p>
                </footer>
            </div>
        </BrowserRouter>

    );
};
