import * as ActionTypes from "../actiontypes/VisitActionTypes"

export default function reducer(state = {
    visits: [],
    petVisits: [],
    vetVisits: [],
    fetching: false,
    fetched: false
}, action) {

    switch (action.type) {
        case ActionTypes.FETCH_VISITS_FULFILLED: {
            const visits = action.payload
            return {
                ...state,
                fetching: false,
                fetched: true,
                visits: visits
            }
        }
        case "FETCH_OWNER_PET_VISITS_FULFILLED": {
            return {
                ...state,
                fetching: false,
                fetched: true,
                petVisits: action.payload
            }
        }
        case "FETCH_VET_VISITS_FULFILLED": {
            return {
                ...state,
                fetching: false,
                fetched: true,
                vetVisits: action.payload,
            }
        }
        case ActionTypes.CREATE_VISIT_FULFILLED: {
            return {
                ...state,
                fetching: false,
                fetched: true
            }
        }
        case ActionTypes.CANCEL_VISIT_FULFILLED:
            return {
                ...state,
                visits: state.visits.filter(visit => action.payload.id !== visit.id),
                petVisits: state.petVisits.filter(visit => action.payload.id !== visit.id),
                vetVisits: state.vetVisits.filter(visit => action.payload.id !== visit.id),
                lastUpdated: Date.now()
            }
        default:
            return state
    }

}
