import {combineReducers} from 'redux'
import {reducer as flashReducer} from "redux-flash"
import owners from './OwnersReducer'
import vets from './VetsReducer'
import visits from './VisitsReducer'
import { reducer as formReducer } from 'redux-form'

const formReset = (state) => {
    return {
        ...state,
        anyTouched: false,
        fields: {},
        values: {}
    }
}

const reducers = {
    owners,
    vets,
    visits,
    flash: flashReducer,
    form: formReducer.plugin({
        "new-pet-visit-form": (state, action) => {
            switch (action.type) {
                case "FETCH_VET_AVAILABLE_TIMES_FULFILLED":
                    return {
                        ...state,
                        values: {
                            ...state.values,
                            availableTimes: action.payload
                        }
                    }
                case "CREATE_VISIT_FULFILLED":
                    return formReset(state)
                default:
                    return state
            }
        },
        "new-pet-form": (state, action) => {
            switch (action.type) {
                case "CREATE_PET_FULFILLED" :
                    return formReset(state)
                default:
                    return state
            }
        },
        "new-vet-form": (state, action) => {
            switch (action.type) {
                case "CREATE_VET_FULFILLED" :
                    return formReset(state)
                default:
                    return state
            }
        }
    })
}

let reducer = combineReducers(reducers)
export default reducer
