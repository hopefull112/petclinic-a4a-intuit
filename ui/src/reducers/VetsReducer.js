import * as ActionTypes from "../actiontypes/VetActionTypes"

const initialState = {
    vets: [],
    fetching: false,
    fetched: false
}

export default function vets(state=initialState, action) {

    switch (action.type) {

        case ActionTypes.FETCH_VETS: {
            return {...state, fetching: true}
        }
        case ActionTypes.FETCH_VETS_FULFILLED: {
            return {
                ...state,
                fetching: false,
                fetched: true,
                vets: action.payload,
            }
        }
        case ActionTypes.FETCH_VET_FULFILLED: {
            const vet = action.payload
            return {
                ...state,
                fetching: false,
                fetched: true,
                vet: vet
            }
        }
        case ActionTypes.CREATE_VET_FULFILLED: {
            return {
                ...state,
                fetching: false,
                fetched: true,
                vets: [...state.vets, action.payload]
            }
        }
        case "API_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        default:
            return state
    }


}