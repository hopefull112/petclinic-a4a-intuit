package com.intuit.demo.web.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
public class VisitRequest {

    private Integer ownerId;
    private Integer petId;
    private Integer vetId;
    private LocalDate visitDate;
    private String visitTime;
    private String description;

}
