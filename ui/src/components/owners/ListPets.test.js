import React from 'react'
import { shallow, mount, render } from 'enzyme';
import ListPets from './ListPets'


describe('<ListPets />', () =>  {

    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<ListPets />);
    })

    it('When no pets should not render any pet rows', () => {
        expect(wrapper.find('tr')).toHaveLength(0);
    });

    it('When pets provided should render pet rows', () => {

        const pet = {
            id: 1,
            name: 'Tiger',
            birthDay: '2020-08-02',
            type: {
                name: 'Dog'
            }
        }
        const pets = [pet];

        wrapper.setProps({pets: pets})
        expect(wrapper.find('tr')).toHaveLength(2);
    });

})
