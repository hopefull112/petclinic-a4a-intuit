package com.intuit.demo.service;

import com.intuit.demo.domain.Owner;
import com.intuit.demo.domain.Pet;
import com.intuit.demo.web.response.PetResponse;
import com.intuit.demo.web.request.OwnerRequest;
import com.intuit.demo.web.request.PetRequest;

import java.util.Collection;

public interface OwnerService {

    Collection<Owner> getOwners(String lastName);
    Owner getOwner(Integer ownerId);
    void addOwner(OwnerRequest ownerRequest);
    Pet addPet(PetRequest petRequest);
    Pet getOwnerPet(Integer ownerId, Integer petId);

}
