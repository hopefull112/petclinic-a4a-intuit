package com.intuit.demo.web.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class VisitResponse {

    private Integer id;
    private String description;
    private LocalDateTime visitDate;
    private String status;
    private OwnerPetResponse pet;
    private OwnerResponse owner;
    private VetResponse vet;

}
