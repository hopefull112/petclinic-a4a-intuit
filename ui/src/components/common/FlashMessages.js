import React from 'react'
import { connect } from 'react-redux'
import { getLatestMessage } from 'redux-flash'
import AlertDismissable from "./AlertDismissable";

function FlashMessages ({ flash }) {
  return (
      <div>
        {
          flash &&
          <AlertDismissable body={flash.message} type={flash.isError?"danger":"success"}/>
        }
      </div>
  )
}

function mapStateToProps (state) {
  return {
    flash: getLatestMessage(state)
  }
}

export default connect(mapStateToProps)(FlashMessages);

