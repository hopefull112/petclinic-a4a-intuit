import React from 'react'
import {connect} from 'react-redux'
import {fetchOwnerPet, fetchOwnerPetVisits} from "../../actions/OwnerActions"
import {cancelVisit} from "../../actions/VisitActions"
import ListVisits from "../visits/ListVisits";

class ShowPetVisits extends React.Component {

    componentWillMount() {
        this.props.dispatch(fetchOwnerPetVisits(this.props.match.params.ownerId, this.props.match.params.petId));
        this.props.dispatch(fetchOwnerPet(this.props.match.params.ownerId, this.props.match.params.petId));
    }

    cancelVisit = (visitId) => {
        this.props.dispatch(cancelVisit(visitId));
    }

    render(){
        return(
            <section>
                <h2>Owner Information</h2>
                <table className='table table-striped'>
                    <tbody>
                    <tr>
                        <th>Owner Name</th>
                        <td>{this.props.pet && this.props.pet.owner.firstName +" "+this.props.pet.owner.lastName }</td>
                    </tr>
                    <tr>
                        <th>Pet Name</th>
                        <td>{this.props.pet && this.props.pet.name}</td>
                    </tr>
                    </tbody>
                </table>
                <h2 className="pt-2">Visits Listing</h2>
                <ListVisits visits={this.props.visits} cancelVisit={this.cancelVisit} hidePets/>
            </section>
        )
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        visits: state.visits.petVisits,
        pet: state.owners.pet
    }
}

export default connect(
    mapStateToProps
)(ShowPetVisits)