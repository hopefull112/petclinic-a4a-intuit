import React from "react";
import {Alert} from "react-bootstrap";

const AlertDismissable = props => {

    return (
        <Alert dismissible variant={props.type}>
          {/*<Alert.Heading>{this.props.heading}</Alert.Heading>*/}
          <p>
            {props.body}
          </p>
        </Alert>
    );
}

export default AlertDismissable;