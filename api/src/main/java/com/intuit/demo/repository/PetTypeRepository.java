package com.intuit.demo.repository;

import com.intuit.demo.domain.PetType;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface PetTypeRepository extends Repository<PetType, Integer> {

    PetType findById(Integer id);

    void save(PetType petType);

    List<PetType> findAll();

}
