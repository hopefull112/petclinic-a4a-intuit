import React from 'react'
import {Route, Switch, Redirect} from 'react-router-dom'
import Owners from '../owners/Owners'
import Owner from '../owners/Owner'
import Vets from '../vets/Vets'
import ShowPetVisits from '../owners/ShowPetVisits'
import NewPetVisit from '../visits/NewPetVisit'
import ShowVetVisits from '../vets/ShowVetVisits'
import Visits from '../visits/Visits'
import ErrorPage from '../errors/ErrorPage'
import NoMatch from '../errors/NoMatch'

export const Routes = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Redirect to="/owners"/>
            </Route>
            <Route exact path="/owners" component={Owners}/>
            <Route exact path="/owners/:ownerId" component={Owner}/>
            <Route exact path="/owners/:ownerId/pets/:petId/visits" component={ShowPetVisits}/>
            <Route exact path="/owners/:ownerId/pets/:petId/visits/new" component={NewPetVisit}/>
            <Route exact path="/vets" component={Vets}/>
            <Route exact path="/vets/:vetId/visits" component={ShowVetVisits}/>
            <Route exact path="/visits" component={Visits}/>
            <Route path='/error' component={ErrorPage} />
            <Route path='*' component={NoMatch} />
        </Switch>
    );
};

export default Routes;