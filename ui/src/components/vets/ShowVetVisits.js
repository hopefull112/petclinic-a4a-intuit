import React from 'react';
import connect from "react-redux/es/connect/connect";
import {fetchVet, fetchVetVisits} from "../../actions/VetActions";
import {cancelVisit} from "../../actions/VisitActions";
import ListVisits from "../visits/ListVisits";

class ShowVetVisits extends React.Component {

    componentWillMount() {
        this.props.dispatch(fetchVetVisits(this.props.match.params.vetId));
        this.props.dispatch(fetchVet(this.props.match.params.vetId));
    }

    cancelVisit = (visitId) => {
        this.props.dispatch(cancelVisit(visitId));
    }

    render(){
        console.log(this.props);
        return(
            <section>
                <h2>Veterinarian Information</h2>
                <table className='table table-striped'>
                    <tbody>
                    <tr>
                        <th>Vet Name</th>
                        <td>{this.props.vet && this.props.vet.firstName +" "+this.props.vet.lastName }</td>
                    </tr>
                    </tbody>
                </table>
                <h2 className="pt-2">Visits Listing</h2>
                <ListVisits visits={this.props.visits} cancelVisit={this.cancelVisit} hideVets/>
            </section>
        )
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        visits: state.visits.vetVisits,
        vet: state.vets.vet
    }
}

export default connect(
    mapStateToProps
)(ShowVetVisits)