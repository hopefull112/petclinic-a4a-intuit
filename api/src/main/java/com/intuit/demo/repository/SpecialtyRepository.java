package com.intuit.demo.repository;

import com.intuit.demo.domain.Specialty;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface SpecialtyRepository extends Repository<Specialty, Integer> {

    void save(Specialty speciality);

    List<Specialty> findAll();

    Specialty findById(Integer id);

}
