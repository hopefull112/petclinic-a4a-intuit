import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import store from './store/store'
import {AdminLayout} from './components/admin/AdminLayout'
import './styles/index.scss'
import 'react-datepicker/dist/react-datepicker.css';

ReactDOM.render(
    <Provider store={store}>
        <AdminLayout />
    </Provider>, document.getElementById('app')
);

