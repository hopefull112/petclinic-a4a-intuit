import React from 'react'
import { Field, reduxForm } from 'redux-form'

const SearchOwner = props => {

    const {handleSubmit, submitting} = props

    return (
        <form onSubmit={handleSubmit}>
            <div className="input-group mb-3">
                <Field name="lastName" component="input" type="text" placeholder="Last name" className="form-control" autoComplete="off"/>
                <div className="input-group-append">
                    <button type="submit" disabled={submitting} className="btn btn-default">Submit</button>
                </div>
            </div>
        </form>
    )
}

export default reduxForm({
    form: 'filter-last-name-form'
})(SearchOwner)