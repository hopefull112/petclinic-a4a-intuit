package com.intuit.demo.web.util;

import com.intuit.demo.domain.Owner;
import com.intuit.demo.domain.Pet;
import com.intuit.demo.domain.Vet;
import com.intuit.demo.domain.Visit;
import com.intuit.demo.web.response.OwnerResponse;
import com.intuit.demo.web.response.OwnerPetResponse;
import com.intuit.demo.web.response.VetResponse;
import com.intuit.demo.web.response.VisitResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ResponseHelper {

    public static Collection<OwnerResponse> createOwnerResponseList(Collection<Owner> owners) {
        List<OwnerResponse> ownerResponses = new ArrayList<>();
        for(Owner owner : owners) {
            ownerResponses.add(createOwnerResponse(owner));
        }
        return ownerResponses;
    }

    public static OwnerResponse createOwnerResponse(Owner owner) {
        OwnerResponse ownerResponse = OwnerResponse.create(owner.getId(), owner.getFirstName(), owner.getLastName());
        ownerResponse.setAddress(owner.getAddress());
        ownerResponse.setCity(owner.getCity());
        ownerResponse.setTelephone(owner.getTelephone());
        for(Pet pet : owner.getPets()) {
            ownerResponse.getPets().add(createPetResponse(pet));
        }
        return ownerResponse;
    }

    public static OwnerPetResponse createPetResponse(Pet pet) {
        OwnerPetResponse dto = OwnerPetResponse.create(pet.getId(), pet.getName());
        dto.setBirthDate(pet.getBirthDate());
        dto.setType(pet.getType());
        dto.setOwner(OwnerResponse.create(pet.getOwner().getId(), pet.getOwner().getFirstName(), pet.getOwner().getLastName()));
        dto.setVisits(createVisitResponseList(pet.getVisits()));
        return dto;
    }

    public static List<VetResponse> createVetResponseList(List<Vet> vets) {
        List<VetResponse> vetResponseList = new ArrayList<>();
        for(Vet vet: vets) {
            vetResponseList.add(createVetResponse(vet));
        }
        return vetResponseList;
    }

    public static VetResponse createVetResponse(Vet vet) {
        VetResponse dto = VetResponse.create(vet.getId(), vet.getFirstName(), vet.getLastName(), vet.getSpecialities());
        dto.setVisits(createVisitResponseList(vet.getVisits()));
        return dto;
    }

    public static List<VisitResponse> createVisitResponseList(List<Visit> visits) {
        List<VisitResponse> dtos = new ArrayList<>();
        for(Visit visit : visits) {
            VisitResponse dto = createVisitResponse(visit);
            dtos.add(dto);
        }

        return dtos;
    }

    private static VisitResponse createVisitResponse(Visit visit) {
        VisitResponse dto = new VisitResponse();
        dto.setId(visit.getId());
        dto.setDescription(visit.getDescription());
        dto.setVisitDate(visit.getDate());
        dto.setStatus(visit.getVisitStatus());
        dto.setPet(OwnerPetResponse.create(visit.getPet().getId(), visit.getPet().getName()));
        dto.setOwner(OwnerResponse.create(visit.getPet().getOwner().getId(), visit.getPet().getOwner().getFirstName(), visit.getPet().getOwner().getLastName()));
        dto.setVet(VetResponse.create(visit.getVet().getId(), visit.getVet().getFirstName(), visit.getVet().getLastName(),visit.getVet().getSpecialities()));
        return dto;
    }

}
