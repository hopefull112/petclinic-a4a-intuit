import axios from "axios";
import {
    flashErrorMessage,
    flashMessage,
    flashSuccessMessage
} from "redux-flash";
import * as ActionTypes from "../actiontypes/VisitActionTypes"

export function fetchVisits(lastname = "") {
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_VISITS});

        axios.get("http://localhost:8082/api/visits")
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_VISITS_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function createVisit(data) {
    return function(dispatch) {
        dispatch({type: ActionTypes.CREATE_VISIT});
        axios.post("http://localhost:8082/api/visit/new", data)
            .then((response) => {
                dispatch({type: ActionTypes.CREATE_VISIT_FULFILLED, payload: response.data})
                dispatch(flashMessage("Visit scheduled for pet: " + response.data.pet.name));
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function cancelVisit(visitId) {
    return function(dispatch) {
        dispatch({type: ActionTypes.CANCEL_VISIT});
        axios.post("http://localhost:8082/api/visit/"+visitId+"/cancel")
            .then((response) => {
                dispatch({type: ActionTypes.CANCEL_VISIT_FULFILLED, payload: response.data})
                dispatch(flashSuccessMessage("Visit canceled for pet: " + response.data.pet.name));
            })
            .catch((err) => {
                // dispatch({type: "API_ERROR", payload: err})
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}