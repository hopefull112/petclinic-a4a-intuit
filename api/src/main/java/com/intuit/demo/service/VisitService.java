package com.intuit.demo.service;

import com.intuit.demo.domain.Visit;
import com.intuit.demo.web.response.VisitResponse;
import com.intuit.demo.web.request.VisitRequest;

import java.util.List;

public interface VisitService {

    Visit createVisit(VisitRequest req);

    List<Visit> getAllVisits();

    List<Visit> getPetVisits(Integer petId);

    List<Visit> getVetVisits(Integer vetId);

    Visit cancelVisit(Integer visitId);
}
