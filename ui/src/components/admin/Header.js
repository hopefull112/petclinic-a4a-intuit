import React from 'react';
import Link from 'react-router-dom/Link';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const Header = () => {
    return (
        <header>
            <Navbar bg="dark" expand="lg" variant="dark">
                <Container>
                    <Navbar.Brand><Link to="/"><h2 className="text-light mb-0 text-uppercase">Petclinic</h2></Link></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Navbar.Text><Link to="/owners">Owners</Link></Navbar.Text>
                            {/*<Navbar.Text><Link to="/pets">Pets</Link></Navbar.Text>*/}
                            <Navbar.Text><Link to="/vets">Veterinarians</Link></Navbar.Text>
                            <Navbar.Text><Link to="/visits">Visits</Link></Navbar.Text>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </header>
    );
};

export default Header;
