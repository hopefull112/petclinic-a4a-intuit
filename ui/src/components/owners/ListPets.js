import React from 'react'
import Link from "react-router-dom/Link";
import moment from "moment"

const ListPets = ({pets, baseUrl}) => {

    if (!pets) {
        return <h3>No Pets</h3>
    }

    const rows = petRows(pets, baseUrl)
    return (
        <section>
            <table className='table table-striped'>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Birth Date</th>
                    <th>Type</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        </section>
    )
}

function petRows(pets, baseUrl) {
    return (pets.map(pet => (
        <tr key={pet.id}>
            <td>{pet.name}</td>
            <td>{moment(pet.birthDate).format('DD MMM YYYY')}</td>
            <td>{pet.type.name}</td>
            <td><Link to={`${baseUrl}/pets/${pet.id}/visits/new`}>Add Visit</Link> | <Link to={`${baseUrl}/pets/${pet.id}/visits`}>Show Visits</Link></td>
        </tr>
    )))
}

export default ListPets