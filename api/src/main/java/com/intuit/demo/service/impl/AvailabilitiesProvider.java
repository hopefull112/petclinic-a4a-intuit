package com.intuit.demo.service.impl;

import com.intuit.demo.web.response.AvailabilityResponse;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Component
public class AvailabilitiesProvider {

    private static final int START_HOUR=8;
    private static final int START_MINUTE=0;
    private static final int END_HOUR=17;
    private static final int END_MINUTE=0;
    private static final int DURATION=30;

    public List<AvailabilityResponse> calculateAvailableTimes(LocalDate visitDate, List<LocalDateTime> unavailableTimes) {

        Set<LocalDateTime> unavailableTimesSet = new HashSet<>();
        unavailableTimes.forEach(t -> unavailableTimesSet.add(t));

        LocalDateTime startTime = LocalDateTime.of(visitDate, LocalTime.of(START_HOUR, START_MINUTE));
        LocalDateTime endTime = LocalDateTime.of(visitDate, LocalTime.of(END_HOUR, END_MINUTE));

        List<AvailabilityResponse> availabilities = new ArrayList<>();
        while(startTime.isBefore(endTime)) {
            if(!unavailableTimesSet.contains(startTime)) {
                availabilities.add(createAvailability(startTime));
            }
            startTime = startTime.plusMinutes(DURATION);
        }

        return availabilities;
    }

    private AvailabilityResponse createAvailability(LocalDateTime visitDate) {
        String value = visitDate.format(DateTimeFormatter.ofPattern("HH:mm", Locale.getDefault()));
        String label = visitDate.format(DateTimeFormatter.ofPattern("hh:mm a", Locale.getDefault()));
        return new AvailabilityResponse(label, value);
    }
}
