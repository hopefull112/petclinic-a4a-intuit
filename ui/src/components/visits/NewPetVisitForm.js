import React from 'react';
import {Field, reduxForm, formValueSelector} from 'redux-form';
import ListAvailableTimes from '../vets/ListAvailableTimes';
import {connect} from 'react-redux';
import {
    renderDatePicker,
    renderSelect, renderTextarea,
    required
} from "../common/ReduxFormCustomRenders"
import {fetchVetAvailableTimes} from "../../actions/VetActions";

let NewPetVisitForm = props => {

        const vetOptions = props.vets.map(opt => ({
            label: opt.firstName + " " + opt.lastName,
            value: opt.id
        }));

        const onVetChange = (inputValue) => {
            if (inputValue && props.visitDate) {
                getVetAvailableTimes(inputValue, props.visitDate)
            }
        }

        const onVisitDateChange = (inputValue) => {
            if (inputValue && props.vet) {
                getVetAvailableTimes(props.vet, inputValue)
            }
        }

        const getVetAvailableTimes = (vet, visitDate) => {
            if (vet && visitDate) {
                props.dispatch(
                    fetchVetAvailableTimes(vet.value, visitDate));
            }
        }

        const tomorrow = () => {
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            return tomorrow
        }

        return (
            <form onSubmit={props.handleSubmit}>
                <div className="row my-3">
                    <div className="col-4">
                        <Field name="vet" component={renderSelect}
                               options={vetOptions} isMulti={false}
                               placeholder="Select a veteranarian..."
                               validate={[required]}
                               onChange={onVetChange}
                        />
                    </div>
                    <div className="col-4">
                        <Field name="visitDate" component={renderDatePicker} type="text"
                               className="form-control"
                               autoComplete="off"
                               placeholderText="Date of visit"
                               dateFormat="MM/dd/yyyy"
                               weekdaysOnly={true}
                               minDate={tomorrow()}
                               validate={[required]}
                               onChange={onVisitDateChange}
                        />
                    </div>

                </div>
                <div className="row my-3">
                    <div className="col-12">
                        <h3> Available Times</h3>
                        <ListAvailableTimes availableTimes={props.availableTimes}/>
                    </div>
                </div>
                <div className="row my-4">
                    <div className="col-12">
                        <h3>Description</h3>
                        <Field name="description" component={renderTextarea}
                               className="form-control"
                               placeholder="Purpose of visit..."
                               validate={[required]}
                        />
                    </div>
                </div>
                <div className="row my-3">
                    <div className="col-1">
                        <button type="button" onClick={props.reset}
                                className="btn btn-default">Reset
                        </button>
                    </div>
                    <div className="col-1">
                        <button type="submit" disabled={props.submitting}
                                className="btn btn-default">Submit
                        </button>
                    </div>
                </div>
            </form>
        )
}

NewPetVisitForm = reduxForm({
    form: 'new-pet-visit-form'
})(NewPetVisitForm);

const selector = formValueSelector('new-pet-visit-form')
NewPetVisitForm = connect(state => {
    const visitDate = selector(state, 'visitDate')
    const vet = selector(state, 'vet')
    const availableTimes = selector(state, 'availableTimes')
    return {
        visitDate,
        availableTimes,
        vet
    }
})(NewPetVisitForm);

export default NewPetVisitForm;