package com.intuit.demo.web.response;

public class PetResponse {

    private OwnerPetResponse pet;

    public OwnerPetResponse getPet() {
        return pet;
    }

    public void setPet(OwnerPetResponse pet) {
        this.pet = pet;
    }
}
