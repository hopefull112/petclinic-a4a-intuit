import React from 'react'
import {connect} from 'react-redux'
import ListOwners from './ListOwners'
import SearchOwner from './SearchOwner'
import {fetchOwners, createPets} from "../../actions/OwnerActions"

class Owners extends React.Component {

    componentWillMount() {
        this.props.dispatch(fetchOwners())
    }

    filterOwners = (values) => {
        this.props.dispatch(fetchOwners(values.lastName))
    }

    render() {
        const {owners} = this.props.owners;
        return (

            <div>
                <h2>Filter Owners</h2>
                <SearchOwner onSubmit={this.filterOwners}/>
                <h2 className="mt-4">Owners listing</h2>
                <ListOwners owners={owners} baseUrl={this.props.match.url}/>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        owners: state.owners
    }
}

export default connect(mapStateToProps)(Owners)