import React from 'react';
import NewPetVisitForm from "./NewPetVisitForm";
import {fetchVets} from "../../actions/VetActions";
import connect from "react-redux/es/connect/connect";
import {fetchOwnerPet} from "../../actions/OwnerActions";
import {createVisit} from "../../actions/VisitActions";

class NewPetVisit extends React.Component {

    componentWillMount() {
        if(!this.props.vets || this.props.vets.length < 1){
            this.props.dispatch(fetchVets())
        }
        if(!this.props.owner || !this.props.pet){
            this.props.dispatch(fetchOwnerPet(this.props.match.params.ownerId, this.props.match.params.petId));
        }
    }

    createNewPetVisit = (values) => {
        const data = {
            ownerId: this.props.match.params.ownerId,
            petId: this.props.match.params.petId,
            visitDate: values.visitDate,
            visitTime: values.visitTime,
            vetId: values.vet.value,
            description: values.description
        }
        this.props.dispatch(createVisit(data));
    }

    render(){
        console.log(this.props);
        return(
            <section>
                <h2>Owner Information</h2>
                <table className='table table-striped'>
                    <tbody>
                    <tr>
                        <th>Owner Name</th>
                        <td>{this.props.pet && this.props.pet.owner.firstName +" "+this.props.pet.owner.lastName }</td>
                    </tr>
                    <tr>
                        <th>Pet Name</th>
                        <td>{this.props.pet && this.props.pet.name}</td>
                    </tr>
                    </tbody>
                </table>
                <h2 className="pt-2">Create a new visit</h2>
                <NewPetVisitForm  onSubmit={this.createNewPetVisit} vets={this.props.vets}/>
            </section>
        )
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        vets: state.vets.vets,
        pet: state.owners.pet
    }
}

export default connect(
    mapStateToProps
)(NewPetVisit)