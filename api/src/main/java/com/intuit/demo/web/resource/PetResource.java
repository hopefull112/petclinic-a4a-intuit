package com.intuit.demo.web.resource;

import com.intuit.demo.domain.Pet;
import com.intuit.demo.domain.Visit;
import com.intuit.demo.web.response.OwnerPetResponse;
import com.intuit.demo.web.response.PetResponse;
import com.intuit.demo.web.response.VisitResponse;
import com.intuit.demo.repository.PetRepository;
import com.intuit.demo.web.request.PetRequest;
import com.intuit.demo.service.OwnerService;
import com.intuit.demo.service.VisitService;
import com.intuit.demo.web.util.ResponseHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(produces = "application/json")
public class PetResource {

    private PetRepository petRepository;
    private OwnerService ownerService;
    private VisitService visitService;

    public PetResource(PetRepository petRepository, OwnerService ownerService, VisitService visitService) {
        this.petRepository = petRepository;
        this.ownerService = ownerService;
        this.visitService = visitService;
    }

    @GetMapping(path = "/owners/{ownerId}/pets", produces = "application/json")
    public Collection<Pet> getAllOwnerPets(@PathVariable("ownerId") Integer ownerId) {
        return this.petRepository.findByOwnerId(ownerId);
    }

    @PostMapping(path = "/owners/{ownerId}/pets", consumes = "application/json")
    public OwnerPetResponse addPet(@PathVariable("ownerId") Integer ownerId, @RequestBody PetRequest petRequest) {
        Pet pet = ownerService.addPet(petRequest);
        return ResponseHelper.createPetResponse(pet);
    }

    @GetMapping(path = "/owners/{ownerId}/pets/{petId}", produces = "application/json")
    public PetResponse getOwnerPet(@PathVariable("ownerId") Integer ownerId, @PathVariable("petId") Integer petId) {
        Pet pet = ownerService.getOwnerPet(ownerId, petId);
        PetResponse response = new PetResponse();
        response.setPet(ResponseHelper.createPetResponse(pet));
        return response;
    }

    @GetMapping(path = "/owners/{ownerId}/pets/{petId}/visits")
    public List<VisitResponse> visits(@PathVariable("ownerId") Integer ownerId, @PathVariable("petId") Integer petId) {
        List<Visit> visits = visitService.getPetVisits(petId);
        return ResponseHelper.createVisitResponseList(visits);
    }

}
