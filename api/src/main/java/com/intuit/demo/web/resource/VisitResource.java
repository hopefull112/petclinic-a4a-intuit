package com.intuit.demo.web.resource;

import com.intuit.demo.domain.Visit;
import com.intuit.demo.web.response.VisitResponse;
import com.intuit.demo.web.request.VisitRequest;
import com.intuit.demo.service.VisitService;
import com.intuit.demo.web.util.ResponseHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class VisitResource {

    private VisitService visitService;

    public VisitResource(VisitService visitService) {
        this.visitService = visitService;
    }

    @GetMapping(path = "/visits", produces = "application/json")
    public List<VisitResponse> getVisits() {
        List<Visit> visits = visitService.getAllVisits();
        return ResponseHelper.createVisitResponseList(visits);

    }

    @PostMapping(path = "/visit/new", produces = "application/json")
    public Visit createVisit(@RequestBody VisitRequest visitRequest) {
        return visitService.createVisit(visitRequest);
    }

    @PostMapping(path = "/visit/{visitId}/cancel", produces = "application/json")
    public Visit cancelVist(@PathVariable("visitId") Integer visitId) {
        return visitService.cancelVisit(visitId);
    }

}
