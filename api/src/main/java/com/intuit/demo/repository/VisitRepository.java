package com.intuit.demo.repository;

import com.intuit.demo.domain.Visit;
import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.Repository;

import java.time.LocalDateTime;
import java.util.List;

public interface VisitRepository extends Repository<Visit, Integer> {

    Visit findById(Integer visitId);

    List<Visit> findAllByVisitStatus(String visitStatus);

    List<Visit> findAllByPet_IdAndVisitStatusIsNot(Integer petId, String status);

    List<Visit> findAllByVet_IdAndVisitStatusIsNot(Integer vetId, String status);

    List<Visit> findAllByVet_IdAndDateBetweenAndVisitStatus(Integer vetId, LocalDateTime start, LocalDateTime end, String status);

    void save(Visit visit) throws DataAccessException;
}
