import React from "react";
import ListVisits from "./ListVisits";
import connect from "react-redux/es/connect/connect";
import {fetchVisits, cancelVisit} from "../../actions/VisitActions";

class Visits extends React.Component {

    componentWillMount() {
        this.props.dispatch(fetchVisits())
    }

    cancelVisit = (visitId) => {
        this.props.dispatch(cancelVisit(visitId));
    }

    render() {
        return (
            <div>
                <h2 className="mt-4">Visits listing</h2>
                <ListVisits baseUrl={this.props.match.url} visits={this.props.visits.visits} cancelVisit={this.cancelVisit}/>
            </div>
        );
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        visits: state.visits
    }
}

export default connect(
    mapStateToProps
)(Visits);
