package com.intuit.demo.web.resource;

import com.intuit.demo.domain.Vet;
import com.intuit.demo.domain.Visit;
import com.intuit.demo.web.response.AvailabilityResponse;
import com.intuit.demo.web.response.VetResponse;
import com.intuit.demo.web.response.VisitResponse;
import com.intuit.demo.web.request.VetRequest;
import com.intuit.demo.service.VetService;
import com.intuit.demo.service.VisitService;
import com.intuit.demo.web.util.ResponseHelper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalDate;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/vets", produces = "application/json")
public class VetResource {

    private VetService vetService;
    private VisitService visitService;

    public VetResource(VetService vetService, VisitService visitService) {
        this.vetService = vetService;
        this.visitService = visitService;
    }

    @GetMapping(produces = "application/json")
    public List<VetResponse> getAllVets() {
        List<Vet> vets = vetService.getVets();
        return ResponseHelper.createVetResponseList(vets);
    }

    @PostMapping(produces = "application/json")
    public VetResponse addVet(@RequestBody VetRequest vetRequest) {
        Vet vet = vetService.addVet(vetRequest);
        return ResponseHelper.createVetResponse(vet);
    }

    @GetMapping(path = "/{vetId}")
    public VetResponse getVet(@PathVariable("vetId") Integer vetId) {
        Vet vet= vetService.getVet(vetId);
        return ResponseHelper.createVetResponse(vet);
    }

    @GetMapping(path = "/{vetId}/visits")
    public List<VisitResponse> visits(@PathVariable("vetId") Integer vetId) {
        List<Visit> visits = visitService.getVetVisits(vetId);
        return ResponseHelper.createVisitResponseList(visits);
    }

    @GetMapping(path = "/{vetId}/availability/{visitDate}")
    public List<AvailabilityResponse> availability(@PathVariable("vetId") Integer vetId,
                                                   @PathVariable("visitDate") @DateTimeFormat(pattern = "MM-dd-yyyy") LocalDate visitDate) {
        return vetService.retrieveAvailableTimes(vetId, visitDate);
    }

}
