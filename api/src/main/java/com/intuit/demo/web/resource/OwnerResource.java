package com.intuit.demo.web.resource;

import com.intuit.demo.domain.Owner;
import com.intuit.demo.web.response.OwnerResponse;
import com.intuit.demo.repository.OwnerRepository;
import com.intuit.demo.service.OwnerService;
import com.intuit.demo.web.util.ResponseHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@CrossOrigin
@RestController
@RequestMapping(path = "/owners", produces = "application/json")
public class OwnerResource {

    private OwnerRepository ownerRepository;
    private OwnerService ownerService;

    public OwnerResource(OwnerRepository ownerRepository, OwnerService ownerService) {
        this.ownerRepository = ownerRepository;
        this.ownerService = ownerService;
    }

    @GetMapping(produces = "application/json")
    public Collection<OwnerResponse> getAllOwners(@RequestParam String lastName) {
        Collection<Owner> owners = ownerService.getOwners(lastName);
        return ResponseHelper.createOwnerResponseList(owners);
    }

    @GetMapping(path = "/{ownerId}")
    public OwnerResponse getOwner(@PathVariable("ownerId") Integer ownerId) {
        Owner owner = ownerService.getOwner(ownerId);
        return ResponseHelper.createOwnerResponse(owner);
    }

}
