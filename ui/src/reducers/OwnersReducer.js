import * as ActionTypes from "../actiontypes/OwnerActionTypes"

let initialState = {
    owners: [],
    fetching: false,
    fetched: false
}

export default function owners(state=initialState, action) {
    switch (action.type) {
        case ActionTypes.FETCH_OWNERS: {
            return {...state, fetching: true}
        }
        case ActionTypes.FETCH_OWNERS_FULFILLED: {
            return {
                ...state,
                fetching: false,
                fetched: true,
                owners: action.payload,
            }
        }
        case ActionTypes.FETCH_OWNER_FULFILLED: {
            const owner = action.payload
            const pets = owner.pets
            delete owner.pets
            return {
                ...state,
                owner: owner,
                ownerPets: pets,
                fetching: false,
                fetched: true
            }
        }
        case ActionTypes.FETCH_OWNER_PET_FULFILLED: {
            const owner = action.payload
            const pet = owner.pet
            return {
                ...state,
                fetching: false,
                fetched: true,
                owner: owner,
                pet: pet
            }
        }
        case ActionTypes.CREATE_PET_FULFILLED: {
            const newPet = action.payload
            return {
                ...state,
                ownerPets: [...state.ownerPets, newPet],
                fetching: false,
                fetched: true
            }
        }
        case "API_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        default:
            return state
    }
}
