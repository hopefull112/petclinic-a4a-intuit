package com.intuit.demo.service;

import com.intuit.demo.domain.Vet;
import com.intuit.demo.web.response.AvailabilityResponse;
import com.intuit.demo.web.request.VetRequest;

import java.time.LocalDate;
import java.util.List;

public interface VetService {

    List<Vet> getVets();
    Vet getVet(Integer vetId);
    Vet addVet(VetRequest req);
    List<AvailabilityResponse> retrieveAvailableTimes(Integer vetId, LocalDate visitDate);

}
