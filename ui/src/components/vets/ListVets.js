import React from 'react'
import Link from "react-router-dom/Link";

const ListVets = ({vets, baseUrl}) => {

    if (!vets.length) {
        return <h3>No vets</h3>
    }

    const rows = vetRows(vets, baseUrl)
    return (
        <section>
            <table className='table table-striped'>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Speciality</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        </section>
    )
}

function vetRows(vets, baseUrl) {
    return (vets.map(vet =>
        <tr key={vet.id}>
            <td>{vet.firstName} {vet.lastName}</td>
            <td>{vet.specialities ? vet.specialities.map(speciality => speciality.name).join(', ') : ""}</td>
            <td><Link to={`${baseUrl}/${vet.id}/visits`}>Show Visits</Link></td>
        </tr>
    ))
}

export default ListVets