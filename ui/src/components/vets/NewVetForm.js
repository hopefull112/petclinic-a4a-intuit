import React from 'react'
import { Field, reduxForm } from 'redux-form'
import {
    renderSelect,
    renderField,
    required,
    minLength2,
    alphaNumeric
} from "../common/ReduxFormCustomRenders"

const NewPetForm = (props) => {

    const {handleSubmit, reset, submitting} = props;
    const specialityOptions = [
        {value: '1', label: 'Primary Care'},
        {value: '2', label: 'Radiology'},
        {value: '3', label: 'Surgery'},
    ];

    return(
        <form onSubmit={handleSubmit}>
            <div className="input-group mb-3">

                <div className="col-3 pl-0">
                    <Field name="firstName" component={renderField} type="text"
                           placeholder="FirstName" className="form-control"
                           validate={[required, minLength2, alphaNumeric]} autoComplete="off"/>
                </div>
                <div className="col-3 pl-0">
                    <Field name="lastName" component={renderField} type="text"
                           placeholder="LastName" className="form-control"
                           validate={[required, minLength2, alphaNumeric]} autoComplete="off"/>
                </div>
                <div className="col-4">
                    <Field name="specialityIds" component={renderSelect}
                           options={specialityOptions} placeholder="Select specialities"
                           isMulti={true}/>
                </div>
                <div className="col-1">
                    <button type="button" onClick={reset}
                            className="btn btn-default">Reset
                    </button>
                </div>
                <div className="col-1">
                    <button type="submit" disabled={submitting}
                            className="btn btn-default">Submit
                    </button>
                </div>
            </div>
        </form>
    )
}

export default reduxForm({
    form: 'new-vet-form'
})(NewPetForm)