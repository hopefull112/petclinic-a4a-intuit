package com.intuit.demo.web.resource;

import com.intuit.demo.domain.PetType;
import com.intuit.demo.domain.Specialty;
import com.intuit.demo.repository.PetTypeRepository;
import com.intuit.demo.repository.SpecialtyRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class TypeResource {

    private PetTypeRepository petTypeRepository;
    private SpecialtyRepository specialtyRepository;

    public TypeResource(PetTypeRepository petTypeRepository, SpecialtyRepository specialtyRepository) {
        this.petTypeRepository = petTypeRepository;
        this.specialtyRepository = specialtyRepository;
    }

    @PostMapping(path = "/pettypes/{name}")
    public void createPetType(@PathVariable String name) {
        PetType petType = new PetType();
        petType.setName(name);
        petTypeRepository.save(petType);
    }

    @GetMapping(path = "/pettypes", produces = "application/json")
    public List<PetType> getAllPetTypes() {
        return petTypeRepository.findAll();
    }

    @PostMapping(path = "/specialties/{name}")
    public void createSpecialty(@PathVariable String name) {
        Specialty sp = new Specialty();
        sp.setName(name);
        specialtyRepository.save(sp);
    }

    @GetMapping(path = "/specialties", produces = "application/json")
    public List<Specialty> getAllSpecialties() {
        return specialtyRepository.findAll();
    }

}
