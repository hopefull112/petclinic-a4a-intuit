package com.intuit.demo.service.impl;

import com.intuit.demo.domain.Pet;
import com.intuit.demo.domain.Vet;
import com.intuit.demo.domain.Visit;
import com.intuit.demo.domain.VisitStatus;
import com.intuit.demo.repository.PetRepository;
import com.intuit.demo.repository.VetRepository;
import com.intuit.demo.repository.VisitRepository;
import com.intuit.demo.service.VisitService;
import com.intuit.demo.web.request.VisitRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Service
public class VisitServiceImpl implements VisitService {

    private VisitRepository visitRepository;
    private PetRepository petRepository;
    private VetRepository vetRepository;

    public VisitServiceImpl(VisitRepository visitRepository, PetRepository petRepository, VetRepository vetRepository) {
        this.visitRepository = visitRepository;
        this.petRepository = petRepository;
        this.vetRepository = vetRepository;
    }

    @Override
    public List<Visit> getAllVisits() {
        return visitRepository.findAllByVisitStatus(VisitStatus.SCHEDULED.name());
    }

    @Override
    public List<Visit> getVetVisits(Integer vetId) {
        return visitRepository.findAllByVet_IdAndVisitStatusIsNot(vetId, VisitStatus.CANCELED.name());
    }

    @Override
    public List<Visit> getPetVisits(Integer petId) {
        return visitRepository.findAllByPet_IdAndVisitStatusIsNot(petId, VisitStatus.CANCELED.name());
    }

    public Visit cancelVisit(Integer visitId) {
        Visit visit = visitRepository.findById(visitId);
        visit.setVisitStatus(VisitStatus.CANCELED.name());
        visitRepository.save(visit);
        return visit;
    }

    @Override
    public Visit createVisit(VisitRequest req) {
        Visit visit = populateVisit(req);
        visitRepository.save(visit);
        return visit;
    }

    private Visit populateVisit(VisitRequest req) {
        Visit visit = new Visit();
        visit.setDescription(req.getDescription());
        visit.setVisitStatus(VisitStatus.SCHEDULED.name());
        String[] time = req.getVisitTime().split(":");
        LocalDateTime ldt = LocalDateTime.of(req.getVisitDate(), LocalTime.of(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
        visit.setDate(LocalDateTime.of(req.getVisitDate(), LocalTime.of(ldt.getHour(), ldt.getMinute())));
        Pet pet = petRepository.findById(req.getPetId());
        pet.addVisit(visit);
        Vet vet = vetRepository.findById(req.getVetId());
        vet.addVisit(visit);
        return visit;
    }
}
