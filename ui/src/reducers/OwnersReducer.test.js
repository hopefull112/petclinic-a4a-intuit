import React from 'react'
import OwnerReducer from './OwnersReducer'
import * as ActionTypes from "../actiontypes/OwnerActionTypes";

describe('', () => {

    it('should return the initial state', () => {

        expect(OwnerReducer(undefined, {type: ActionTypes.FETCH_OWNERS})).toEqual({
            owners: [],
            fetching: true,
            fetched: false
        })
    });

    it('should return the initial state', () => {

        const currState = {
            owners: [],
            fetching: true,
            fetched: false
        }

        const expectedState = {
            owners: [1,2],
            fetching: false,
            fetched: true
        }

        const action = {
            type: ActionTypes.FETCH_OWNERS_FULFILLED,
            payload: [1,2]
        }

        expect(OwnerReducer(currState, action)).toEqual(expectedState)
    })

})