package com.intuit.demo.web.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VetRequest {

    public String firstName;
    public String lastName;
    public String specialityIds;

}
