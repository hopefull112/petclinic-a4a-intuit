import React from 'react'
import Link from "react-router-dom/Link";

const ListOwners = ({owners, baseUrl}) => {

    if (!owners.length) {
        return <h3>No owners</h3>
    }

    const rows = ownerRows(owners, baseUrl)
    return (
        <section>
            <table className='table table-striped'>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>Telephone</th>
                    <th>Pets</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        </section>
    )
}

function ownerRows(owners, baseUrl) {
    return (owners.map(owner => (
        <tr key={owner.id}>
            <td><Link
                to={baseUrl + '/' + owner.id}>{owner.firstName} {owner.lastName}</Link>
            </td>
            <td>{owner.address}</td>
            <td>{owner.city}</td>
            <td>{owner.telephone}</td>
            <td>{owner.pets.map(pet => pet.name).join(', ')}</td>
        </tr>
    )))
}

export default ListOwners