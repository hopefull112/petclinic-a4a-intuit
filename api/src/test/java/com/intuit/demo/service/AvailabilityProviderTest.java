package com.intuit.demo.service;

import com.intuit.demo.service.impl.AvailabilitiesProvider;
import com.intuit.demo.web.response.AvailabilityResponse;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AvailabilityProviderTest {

    private AvailabilitiesProvider provider = new AvailabilitiesProvider();

    @Test
    public void whenAllSlotsAreAvailable_thenAllSlotsAreReturned() {
        LocalDate date = LocalDate.now().plusDays(1);
        List<LocalDateTime> unAvailableTimes = new ArrayList<>();
        List<AvailabilityResponse> availabilities = provider.calculateAvailableTimes(date, unAvailableTimes);
        assertEquals(18, availabilities.size());
    }

    @Test
    public void whenFirstSlotIsTaken_thenAllRemainingSlotsAreReturned() {
        LocalDate date = LocalDate.now().plusDays(1);
        List<LocalDateTime> unAvailableTimes = new ArrayList<>();
        LocalDateTime bookedSlot = LocalDateTime.of(date, LocalTime.of(8, 0));
        unAvailableTimes.add(bookedSlot);
        List<AvailabilityResponse> availabilities = provider.calculateAvailableTimes(date, unAvailableTimes);
        assertEquals(17, availabilities.size());
        String slot = bookedSlot.format(DateTimeFormatter.ofPattern("hh:mm", Locale.getDefault()));
        assertEquals(0, availabilities.stream().filter(a -> slot.equals(a.getValue())).collect(Collectors.toList()).size());
    }


}
