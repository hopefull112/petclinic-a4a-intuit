package com.intuit.demo.web.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AvailabilityResponse {
    private String value;
    private String label;

    public AvailabilityResponse(String label, String value){
        this.label = label;
        this.value = value;
    }
}
