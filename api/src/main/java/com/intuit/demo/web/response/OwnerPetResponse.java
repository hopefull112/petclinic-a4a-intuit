package com.intuit.demo.web.response;

import com.intuit.demo.domain.PetType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class OwnerPetResponse {

    private Integer id;
    private String name;
    private LocalDate birthDate;
    private PetType type;
    private OwnerResponse owner;
    private List<VisitResponse> visits = new ArrayList<>();

    public static OwnerPetResponse create(Integer id, String name) {
        OwnerPetResponse dto = new OwnerPetResponse();
        dto.setId(id);
        dto.setName(name);
        return dto;
    }
}
