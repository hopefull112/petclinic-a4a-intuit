import axios from "axios";
import {flashErrorMessage, flashSuccessMessage} from "redux-flash";
import * as ActionTypes from "../actiontypes/OwnerActionTypes"

export function fetchOwners(lastname = "") {
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_OWNERS});

        axios.get("http://localhost:8082/api/owners?lastName="+lastname)
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_OWNERS_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function fetchOwner(ownerId) {
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_OWNER});

        axios.get("http://localhost:8082/api/owners/"+ownerId)
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_OWNER_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function createPet(data) {
    return function(dispatch) {
        dispatch({type: ActionTypes.CREATE_PET});
        axios.post("http://localhost:8082/api/owners/"+data.ownerId+"/pets", data)
            .then((response) => {
                dispatch({type: ActionTypes.CREATE_PET_FULFILLED, payload: response.data})
                dispatch(flashSuccessMessage("Pet added: " + response.data.name));
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function fetchOwnerPet(ownerId, petId) {
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_OWNER_PET});

        axios.get("http://localhost:8082/api/owners/"+ownerId+"/pets/"+petId)
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_OWNER_PET_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}

export function fetchOwnerPetVisits(ownerId, petId) {
    return function(dispatch) {
        dispatch({type: ActionTypes.FETCH_OWNER_PET_VISITS});

        axios.get("http://localhost:8082/api/owners/"+ownerId+"/pets/"+petId+"/visits")
            .then((response) => {
                dispatch({type: ActionTypes.FETCH_OWNER_PET_VISITS_FULFILLED, payload: response.data})
            })
            .catch((err) => {
                dispatch(flashErrorMessage("API Error: " + err.message));
            })
    }
}