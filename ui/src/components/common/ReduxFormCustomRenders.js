import React from "react";
import DatePicker from "react-datepicker";
import Select from "react-select";
import moment from "moment";

export const required = value => (value || typeof value === 'number' ? undefined : 'Required')
export const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
export const maxLength15 = maxLength(15)
export const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined
export const minLength2 = minLength(2)
export const number = value =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined
export const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined
export const minValue13 = minValue(13)
export const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email address'
        : undefined
export const tooYoung = value =>
    value && value < 13
        ? 'You do not meet the minimum age requirement!'
        : undefined
export const alphaNumeric = value =>
    value && /[^a-zA-Z0-9]/i.test(value)
        ? 'Only alphanumeric characters'
        : undefined
export const phoneNumber = value =>
    value && !/^(0|[1-9][0-9]{9})$/i.test(value)
        ? 'Invalid phone number, must be 10 digits'
        : undefined

const weekdayFilter = (value) => {
  return (moment(value).day()>0 && moment(value).day()<6)
}

const allDays = (value) => {
  return true
}

export const renderField = (props) => (
    <span>
      <input {...props.input} placeholder={props.placeholder} type={props.type} className={props.className}/>
        {props.meta.touched && props.meta.error && <span className="text-danger">{props.meta.error}</span>}
    </span>
)

export const renderTextarea = (props) => (
    <span>
      <textarea {...props.input} placeholder={props.placeholder} className={props.className} rows={props.rows}/>
      {props.meta.touched && props.meta.error && <span className="text-danger">{props.meta.error}</span>}
    </span>
)

export const renderDatePicker = (props) => {
  return (
      <span>
        <DatePicker
            {...props}
            selected={props.input.value || null}
            onChange={props.input.onChange}
            filterDate={props.weekdaysOnly?weekdayFilter:allDays}
        />
        {props.meta.touched && props.meta.error && <span className="text-danger d-block">{props.meta.error}</span>}
      </span>
)
}

export const renderSelect = (props) => {
  return (
      <span>
        <Select
          {...props}
          value={props.input.value || null}
          onChange={props.input.onChange}
        />
        {props.meta.touched && props.meta.error && <span className="text-danger d-block">{props.meta.error}</span>}
      </span>

)
}