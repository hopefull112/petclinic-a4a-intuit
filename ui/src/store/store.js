import { applyMiddleware, createStore } from "redux"

import logger from "redux-logger"
import thunk from "redux-thunk"
import {middleware as flashMiddleware} from "redux-flash"
import flashMessageOptions from "./flashMessageOptions"

import reducer from "../reducers/index"

const middleware = applyMiddleware(thunk, logger, flashMiddleware(flashMessageOptions))

export default createStore(reducer, middleware)
