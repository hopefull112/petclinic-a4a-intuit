# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a Pet Clinic demo application. Frontend is implemented in React. Backend is Spring Boot application exposed
as REST API and using H2 in memory db.
* Version 1.0

### How do I get set up? ###

* UI folder contians the project for frontend. 
* API folder contains the project for the backend. 
* Import the root folder as project into IDE. Then IDE should recognize ui folder containing web project and api 
folder containing a java project. To run ui go into ui folder and execute: npm run start. To run the backend go 
into the api folder and then right click on the project and run the application. 
* Database is in memory db. Open the db from browser at url: http://localhost:8082/api/h2-console. In order to use 
the application some db setup needs to be done. Please open the file api/src/main/resources/dbsetup.sql. Copy the sql
from that file and run that in the db console. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact