package com.intuit.demo.web.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PetRequest {

    private String name;
    private LocalDate birthDate;
    private Integer typeId;
    private Integer ownerId;

}
