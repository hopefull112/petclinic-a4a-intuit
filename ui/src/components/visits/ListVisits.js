import React from "react";
import moment from "moment"
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";

const ListVisits = props => {

        const {visits, hidePets, hideVets, cancelVisit} = props

        if (!visits || visits.length < 1) {
            return <h3>No visits</h3>
        }

        const mappedVisits = visits.map(visit =>
            <tr key={visit.id}>
                {!hidePets &&  <td>{visit.owner.firstName} {visit.owner.lastName}</td>}
                {!hidePets &&  <td>{visit.pet.name}</td>}
                {!hideVets &&  <td>{visit.vet.firstName} {visit.vet.lastName}</td>}
                <td>{moment(visit.visitDate).format('DD MMM YYYY, hh:mm a')}</td>
                <td>{visit.description}</td>
                <td>
                    <ButtonGroup size="sm">
                        <Button variant="default" onClick={() => cancelVisit(visit.id)}>Cancel</Button>
                    </ButtonGroup>
                </td>
            </tr>
        );

        return (
            <section>
                <table className='table table-striped'>
                    <thead>
                    <tr>
                        {!hidePets && <th>Owner</th>}
                        {!hidePets && <th>Pet</th>}
                        {!hideVets && <th>Vet</th> }
                        <th>Visit Date</th>
                        <th>Descriptions</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {mappedVisits}
                    </tbody>
                </table>
            </section>
        );
}

export default ListVisits;